import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DatabaseConfigService } from '../database/database.config.service';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  constructor(
    private readonly configService: ConfigService,
    private readonly databaseConfigService: DatabaseConfigService,
  ) {}
  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: this.databaseConfigService.vendor,
      ssl: this.databaseConfigService.ssl,
      host: this.databaseConfigService.host,
      port: this.databaseConfigService.port,
      username: this.databaseConfigService.username,
      password: this.databaseConfigService.password,
      database: this.databaseConfigService.name,
      poolSize: this.configService.get('typeOrm.poolSize'),
      retryAttempts: this.configService.get('typeOrm.retryAttempts'),
      retryDelay: this.configService.get('typeOrm.retryDelay'),
      autoLoadEntities: true,
      synchronize: this.configService.get('typeOrm.synchronize'),
    };
  }
}
