import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import typeOrmConfig from './type.orm.config';
import * as Joi from 'joi';
import { TypeOrmConfigService } from './type.orm.config.service';
import { DatabaseConfigModule } from '../database/database.config.module';
import { DatabaseConfigService } from '../database/database.config.service';
import { TypeOrmPoolSizeService } from './type.orm.pool.size.service';

@Module({
  imports: [
    DatabaseConfigModule,
    ConfigModule.forRoot({
      envFilePath: '.env.development.cloud',
      load: [typeOrmConfig],
      validationSchema: Joi.object({
        TYPE_ORM_RETRY_ATTEMPTS: Joi.number().required(),
        TYPE_ORM_RETRY_DELAY: Joi.number().required(),
        TYPE_ORM_SYNCHRONIZE: Joi.boolean().required(),
        TYPE_ORM_POOL_SIZE: Joi.number().required(),
      }),
    }),
  ],
  providers: [
    DatabaseConfigService,
    TypeOrmConfigService,
    TypeOrmPoolSizeService,
  ],
  exports: [TypeOrmConfigService],
})
export class TypeOrmConfigModule {}
