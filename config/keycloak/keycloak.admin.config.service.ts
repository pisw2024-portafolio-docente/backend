import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class KeycloakAdminConfigService {
  constructor(private readonly configService: ConfigService) {}
  get baseUrl() {
    return this.configService.get('keycloak.serverUrl');
  }
  get masterRealm() {
    return this.configService.get('keycloak.masterRealm');
  }
  get masterUsername() {
    return this.configService.get('keycloak.masterUsername');
  }
  get masterPassword() {
    return this.configService.get('keycloak.masterPassword');
  }
  get applicationRealm() {
    return this.configService.get('keycloak.realmName');
  }
}
