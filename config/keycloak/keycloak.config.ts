import { registerAs } from '@nestjs/config';

export default registerAs('keycloak', () => ({
  serverUrl: process.env.KEYCLOAK_SERVER_URL,
  realmName: process.env.KEYCLOAK_REALM_NAME,
  clientId: process.env.KEYCLOAK_CLIENT_ID,
  clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
  masterRealm: process.env.KEYCLOAK_MASTER_REALM,
  masterUsername: process.env.KEYCLOAK_MASTER_USERNAME,
  masterPassword: process.env.KEYCLOAK_MASTER_PASSWORD,
}));
