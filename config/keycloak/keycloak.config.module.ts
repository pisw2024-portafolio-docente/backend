import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import keycloakConfig from './keycloak.config';
import * as Joi from 'joi';
import { KeycloakConfigService } from './keycloak.config.service';
import { KeycloakAdminConfigService } from './keycloak.admin.config.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env.development.cloud',
      load: [keycloakConfig],
      validationSchema: Joi.object({
        KEYCLOAK_SERVER_URL: Joi.string().required(),
        KEYCLOAK_REALM_NAME: Joi.string().required(),
        KEYCLOAK_CLIENT_ID: Joi.string().required(),
        KEYCLOAK_CLIENT_SECRET: Joi.string().required(),
        KEYCLOAK_MASTER_REALM: Joi.string().required(),
        KEYCLOAK_MASTER_USERNAME: Joi.string().required(),
        KEYCLOAK_MASTER_PASSWORD: Joi.string().required(),
      }),
    }),
  ],
  providers: [KeycloakConfigService, KeycloakAdminConfigService],
  exports: [KeycloakConfigService, KeycloakAdminConfigService],
})
export class KeycloakConfigModule {}
