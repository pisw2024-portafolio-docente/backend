import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DatabaseConfigService {
  constructor(private readonly configService: ConfigService) {}
  get vendor() {
    return this.configService.get('database.vendor');
  }
  get ssl() {
    return this.configService.get('database.ssl');
  }
  get host() {
    return this.configService.get('database.host');
  }
  get port() {
    return this.configService.get('database.port');
  }
  get name() {
    return this.configService.get('database.name');
  }
  get username() {
    return this.configService.get('database.username');
  }
  get password() {
    return this.configService.get('database.password');
  }
}
