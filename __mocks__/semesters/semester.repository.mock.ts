import { UpdateSemesterDto } from "../../src/semesters/dto/update-semester.dto";
import { CreateSemesterDto } from "../../src/semesters/dto/create-semester.dto";
import { SemesterEntity } from "../../src/semesters/persistence/semester.entity";

export class SemesterRepositoryMock {
  semesters: SemesterEntity[] = [];
  private idSequence = 1;
  async getAllSemesters(): Promise<SemesterEntity[]> {
    return this.semesters;
  }
  async getSemesterById(id: number): Promise<SemesterEntity> {
    return this.semesters.find(semester => semester.getId() === id);
  }
  async createSemester(createSemesterDto: CreateSemesterDto): Promise<void> {
    const newSemester = new SemesterEntity(
      this.idSequence++,
      createSemesterDto.getName(),
      createSemesterDto.getYear(),
      createSemesterDto.getStartAt(),
      createSemesterDto.getEndsAt()
    );
    this.semesters.push(newSemester);
  }
  async updateSemester(id: number, updateSemesterDto: UpdateSemesterDto): Promise<void> {
    const index = this.semesters.findIndex(semester => semester.getId() === id);
    if (index !== -1) {
      this.semesters[index] = new SemesterEntity(
        id,
        updateSemesterDto.getName(),
        updateSemesterDto.getYear(),
        updateSemesterDto.getStartAt(),
        updateSemesterDto.getEndsAt()
      );
    }
  }
  async deleteSemesterById(id: number): Promise<void> {
    this.semesters = this.semesters.filter(semester => semester.getId() !== id);
  }
}
