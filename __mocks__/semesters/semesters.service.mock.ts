import { UpdateSemesterDto } from "../../src/semesters/dto/update-semester.dto";
import { CreateSemesterDto } from "../../src/semesters/dto/create-semester.dto";
import { SemesterEntity } from "../../src/semesters/persistence/semester.entity";
import { NotFoundException } from "@nestjs/common";

export class SemestersServiceMock {
  private semesters: SemesterEntity[] = [];
  private idSequence = 1;
  async findAll(): Promise<SemesterEntity[]> {
    return this.semesters;
  }
  async findOne(id: number): Promise<SemesterEntity> {
    const semester = this.semesters.find(semester => semester.getId() === id);
    if (!semester) {
      throw new NotFoundException(`Semester with id = ${id} does not exists!`);
    }
    return semester;
  }
  async createOne(createSemesterDto: CreateSemesterDto): Promise<void> {
    const newSemester = new SemesterEntity(
      this.idSequence++,
      createSemesterDto.getName(),
      createSemesterDto.getYear(),
      createSemesterDto.getStartAt(),
      createSemesterDto.getEndsAt()
    );
    this.semesters.push(newSemester);
  }
  async update(id: number, updateSemesterDto: UpdateSemesterDto): Promise<void> {
    const index = this.semesters.findIndex(semester => semester.getId() === id);
    if (index === -1) {
      throw new NotFoundException(`Semester with id = ${id} does not exists!`);
    }
    this.semesters[index] = new SemesterEntity(
      id,
      updateSemesterDto.getName(),
      updateSemesterDto.getYear(),
      updateSemesterDto.getStartAt(),
      updateSemesterDto.getEndsAt()
    );
  }
  async remove(id: number): Promise<void> {
    const index = this.semesters.findIndex(semester => semester.getId() === id);
    if (index === -1) {
      throw new NotFoundException(`Semester with id = ${id} does not exists!`);
    }
    this.semesters.splice(index, 1);
  }
}
