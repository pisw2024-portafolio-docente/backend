import UserRepresentation from "@keycloak/keycloak-admin-client/lib/defs/userRepresentation";
import { NotFoundException } from "@nestjs/common";
import { CreateUserDto } from "../../src/users/dto/create-user.dto";
import { KeycloakAdminConfigServiceMock } from "./kc.admin.config.service.mock";

export class UsersServiceMock {
  private users: UserRepresentation[] = [
    {
      id: '1',
      username: 'user1',
      firstName: 'First',
      lastName: 'Last',
      email: 'user1@example.com',
      enabled: true,
    },
    {
      id: '2',
      username: 'user2',
      firstName: 'Second',
      lastName: 'Last',
      email: 'user2@example.com',
      enabled: true,
    },
  ];
  private keycloakAdminConfigService = new KeycloakAdminConfigServiceMock();
  async findAll() {
    return this.users.map(user => this.mapKeycloakUserToAppUser(user));
  }
  async findOne(id: string) {
    const user = this.users.find(user => user.id === id);
    if (!user) {
      throw new NotFoundException(`User with id = ${id} does not exist!`);
    }
    return this.mapKeycloakUserToAppUser(user);
  }
  async create(createUserDto: CreateUserDto) {
    const newUser: UserRepresentation = {
      id: (this.users.length + 1).toString(),
      username: createUserDto.username,
      firstName: createUserDto.firstName,
      lastName: createUserDto.lastName,
      email: createUserDto.email,
      enabled: true,
    };
    this.users.push(newUser);
    return newUser.id;
  }
  private mapKeycloakUserToAppUser(userRepresentation: UserRepresentation) {
    return {
      id: userRepresentation.id,
      username: userRepresentation.username,
      firstName: userRepresentation.firstName,
      lastName: userRepresentation.lastName,
      email: userRepresentation.email,
      enabled: userRepresentation.enabled,
    };
  }
}
