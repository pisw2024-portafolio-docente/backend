import { ConnectionConfig } from "@keycloak/keycloak-admin-client/lib/client";
import UserRepresentation from "@keycloak/keycloak-admin-client/lib/defs/userRepresentation";
import { Credentials } from "@keycloak/keycloak-admin-client/lib/utils/auth";

export class KcAdminClientMock {
  private config: ConnectionConfig;
  private users: UserRepresentation[] = [];
  async auth(credentials: Credentials) {
    console.log(credentials);
  }
  setConfig(config: ConnectionConfig) {
    this.config = config;
  }
  async usersFind() {
    return this.users;
  }
  async usersFindOne({ id }: { id: string }) {
    return this.users.find(user => user.id === id) || null;
  }
  async usersCreate(user: any) {
    const newUser = { ...user, id: 'mock-id' };
    this.users.push(newUser);
    return newUser.id;
  }
}
