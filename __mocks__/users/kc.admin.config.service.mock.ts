import { KeycloakAdminConfigService } from '../../config/keycloak/keycloak.admin.config.service';

export class KeycloakAdminConfigServiceMock {
  get baseUrl() {
    return 'http://mocked-keycloak-url';
  }
  get masterRealm() {
    return 'master';
  }
  get masterUsername() {
    return 'mocked-master-username';
  }
  get masterPassword() {
    return 'mocked-master-password';
  }
  get applicationRealm() {
    return 'application-realm';
  }
}
