import { Module } from "@nestjs/common";
import { KeycloakAdminConfigService } from "../../config/keycloak/keycloak.admin.config.service";
import { KeycloakAdminConfigServiceMock } from "./kc.admin.config.service.mock";

@Module({
  providers: [
    {
      provide: KeycloakAdminConfigService,
      useClass: KeycloakAdminConfigServiceMock,
    },
  ],
  exports: [KeycloakAdminConfigService],
})
export class KeycloakConfigTestModule {}