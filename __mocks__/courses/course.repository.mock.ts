import { CreateCourseDto } from "../../src/courses/dto/create-course.dto";
import { UpdateCourseDto } from "../../src/courses/dto/update-course.dto";
import { CourseEntity } from "../../src/courses/persistence/course.entity";

export class CourseRepositoryMock {
  courses: CourseEntity[] = [];
  private idSequence: number = 1;
  async getAllCourses(): Promise<CourseEntity[]> {
    return this.courses;
  }
  async getCourseById(id: number): Promise<CourseEntity> {
    return this.courses.find(course => course.id === id);
  }
  async createCourse(createCourseDto: CreateCourseDto): Promise<CourseEntity> {
    const id = this.idSequence++;
    const newCourse = new CourseEntity(id, createCourseDto.getName(), createCourseDto.getSyllabus());
    this.courses.push(newCourse);
    return newCourse;
  }
  async createCourses(createCourseDtos: CreateCourseDto[]): Promise<CourseEntity[]> {
    const newCourses = createCourseDtos.map(dto => {
      const id = this.idSequence++;
      const newCourse = new CourseEntity(id, dto.getName(), dto.getSyllabus());
      this.courses.push(newCourse);
      return newCourse;
    });
    return newCourses;
  }
  async updateCourse(id: number, updateCourseDto: UpdateCourseDto): Promise<void> {
    const index = this.courses.findIndex(course => course.id === id);
    if (index > -1) {
      this.courses[index] = Object.assign(new CourseEntity(this.courses[index].id, this.courses[index].getName(), this.courses[index].getSyllabus()), this.courses[index], updateCourseDto);
    }
  }
  async deleteCourse(id: number): Promise<void> {
    this.courses = this.courses.filter(course => course.id !== id);
  }
}
