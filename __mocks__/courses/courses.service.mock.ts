import { NotFoundException } from '@nestjs/common';
import { CreateCourseDto } from '../../src/courses/dto/create-course.dto';
import { UpdateCourseDto } from '../../src/courses/dto/update-course.dto';
import { CourseEntity } from '../../src/courses/persistence/course.entity';

export class CoursesServiceMock {
  private courses: CourseEntity[] = [];
  private idSequence: number = 1;
  async findAll(): Promise<CourseEntity[]> {
    return this.courses;
  }
  async findOne(id: number): Promise<CourseEntity> {
    const course = this.courses.find(course => course.getId() === id);
    if (!course) {
      throw new NotFoundException(`Course with id = ${id} does not exist!`);
    }
    return course;
  }
  async create(createCourseDto: CreateCourseDto): Promise<CourseEntity> {
    const id = this.idSequence++;
    const newCourse = new CourseEntity(id, createCourseDto.getName(), createCourseDto.getSyllabus());
    this.courses.push(newCourse);
    return newCourse;
  }
  async update(id: number, updateCourseDto: UpdateCourseDto): Promise<void> {
    const index = this.courses.findIndex(course => course.getId() === id);
    if (index === -1) {
      throw new NotFoundException(`Course with id = ${id} does not exist!`);
    }
    const existingCourse = this.courses[index];
    this.courses[index] = new CourseEntity(
      existingCourse.getId(),
      updateCourseDto.getName() || existingCourse.getName(),
      updateCourseDto.getSyllabus() || existingCourse.getSyllabus()
    );
  }
  async remove(id: number): Promise<void> {
    const index = this.courses.findIndex(course => course.getId() === id);
    if (index === -1) {
      throw new NotFoundException(`Course with id = ${id} does not exist!`);
    }
    this.courses.splice(index, 1);
  }
}
