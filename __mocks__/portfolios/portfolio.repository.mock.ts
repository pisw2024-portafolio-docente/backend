import { NotFoundException } from "@nestjs/common";
import { CreatePortfolioDto } from "../../src/portfolios/dto/create-portfolio.dto";
import { UpdatePortfolioDto } from "../../src/portfolios/dto/update-portfolio.dto";
import { PortfolioEntity } from "../../src/portfolios/persistence/portfolio.entity";

export class PortfolioRepositoryMock {
  portfolios: PortfolioEntity[] = [];
  private idSequence: number = 1;
  async getAllPortfolios(): Promise<PortfolioEntity[]> {
    return this.portfolios;
  }
  async getPortfolioById(id: number): Promise<PortfolioEntity> {
    const portfolio = this.portfolios.find(p => p.getId() === id);
    if (!portfolio) {
      throw new NotFoundException(`Portfolio with id = ${id} does not exist!`);
    }
    return portfolio;
  }
  async createPortfolio(createPortfolioDto: CreatePortfolioDto): Promise<PortfolioEntity> {
    const id = this.idSequence++;
    const newPortfolio = new PortfolioEntity(
      id,
      createPortfolioDto.getCourse(),
      createPortfolioDto.getName(),
      createPortfolioDto.getStudentsAmmount()
    );
    this.portfolios.push(newPortfolio);
    return newPortfolio;
  }
  async updatePortfolio(id: number, updatePortfolioDto: UpdatePortfolioDto): Promise<void> {
    const index = this.portfolios.findIndex(p => p.getId() === id);
    if (index === -1) {
      throw new NotFoundException(`Portfolio with id = ${id} does not exist!`);
    }
    const existingPortfolio = this.portfolios[index];
    this.portfolios[index] = new PortfolioEntity(
      existingPortfolio.getId(),
      updatePortfolioDto.getCourse() || existingPortfolio.getCourse(),
      updatePortfolioDto.getName() || existingPortfolio.getName(),
      updatePortfolioDto.getStudentsAmmount() || existingPortfolio.getStudentsAmount()
    );
  }
  async deletePortfolioById(id: number): Promise<void> {
    const index = this.portfolios.findIndex(p => p.getId() === id);
    if (index === -1) {
      throw new NotFoundException(`Portfolio with id = ${id} does not exist!`);
    }
    this.portfolios.splice(index, 1);
  }
}
