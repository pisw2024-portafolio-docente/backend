import { NotFoundException } from "@nestjs/common";
import { CreatePortfolioDto } from "../../src/portfolios/dto/create-portfolio.dto";
import { UpdatePortfolioDto } from "../../src/portfolios/dto/update-portfolio.dto";
import { PortfolioEntity } from "../../src/portfolios/persistence/portfolio.entity";

export class PortfoliosServiceMock {
  portfolios: PortfolioEntity[] = [
    new PortfolioEntity(1, 'Course1', 'Portfolio1', 30),
    new PortfolioEntity(2, 'Course2', 'Portfolio2', 20),
  ];
  async findAll(): Promise<PortfolioEntity[]> {
    return this.portfolios;
  }
  async findOne(id: number): Promise<PortfolioEntity> {
    const portfolio = this.portfolios.find(p => p.getId() === id);
    if (!portfolio) throw new NotFoundException(`Portfolio with id = ${id} does not exist!`);
    return portfolio;
  }
  async create(createPortfolioDto: CreatePortfolioDto): Promise<PortfolioEntity> {
    const newPortfolio = new PortfolioEntity(
      this.portfolios.length + 1,
      createPortfolioDto.getCourse(),
      createPortfolioDto.getName(),
      createPortfolioDto.getStudentsAmmount()
    );
    this.portfolios.push(newPortfolio);
    return newPortfolio;
  }
  async update(id: number, updatePortfolioDto: UpdatePortfolioDto): Promise<PortfolioEntity> {
    const index = this.portfolios.findIndex(p => p.getId() === id);
    if (index === -1) throw new NotFoundException(`Portfolio with id = ${id} does not exist!`);
    const updatedPortfolio = new PortfolioEntity(
      id,
      updatePortfolioDto.getCourse(),
      updatePortfolioDto.getName(),
      updatePortfolioDto.getStudentsAmmount()
    );
    this.portfolios[index] = updatedPortfolio;
    return updatedPortfolio;
  }
  async remove(id: number): Promise<void> {
    const index = this.portfolios.findIndex(p => p.getId() === id);
    if (index === -1) throw new NotFoundException(`Portfolio with id = ${id} does not exist!`);
    this.portfolios.splice(index, 1);
  }
}
