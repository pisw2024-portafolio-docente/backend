import { Test, TestingModule } from '@nestjs/testing';
import { CoursesController } from './courses.controller';
import { CoursesService } from './courses.service';
import { CoursesServiceMock } from '../../__mocks__/courses/courses.service.mock';
import { NotFoundException } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { CourseEntity } from './persistence/course.entity';

describe('CoursesController', () => {
  let controller: CoursesController;
  let service: CoursesServiceMock;
  beforeEach(async () => {
    service = new CoursesServiceMock();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CoursesController],
      providers: [
        {
          provide: CoursesService,
          useValue: service,
        },
      ],
    }).compile();
    controller = module.get<CoursesController>(CoursesController);
  });
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findAll()', () => {
    it('should return an array of courses', async () => {
      const course1 = new CourseEntity(1, 'Course 1', 'Syllabus 1');
      const course2 = new CourseEntity(2, 'Course 2', 'Syllabus 2');
      service['courses'] = [course1, course2]; // Set up the mock data
      const result = await controller.findAll();
      expect(result).toEqual([course1, course2]);
    });
  });
  describe('findOne()', () => {
    it('should return a course by id', async () => {
      const course = new CourseEntity(1, 'Course 1', 'Syllabus 1');
      service['courses'] = [course];
      const result = await controller.findOne(1);
      expect(result).toEqual(course);
    });
    it('should throw NotFoundException if course not found', async () => {
      await expect(controller.findOne(999)).rejects.toThrow(NotFoundException);
    });
  });
  describe('create()', () => {
    it('should create a new course', async () => {
      const createCourseDto = new CreateCourseDto('Course 1', 'Syllabus 1');
      const newCourse = new CourseEntity(1, 'Course 1', 'Syllabus 1');
      service.create = jest.fn().mockResolvedValue(newCourse);
      const result = await controller.create(createCourseDto);
      expect(result).toEqual(newCourse);
      expect(service.create).toHaveBeenCalledWith(createCourseDto);
    });
  });
  describe('update()', () => {
    it('should update an existing course', async () => {
      const existingCourse = new CourseEntity(1, 'Course 1', 'Syllabus 1');
      const updateCourseDto = new UpdateCourseDto(
        'Updated Course',
        'Updated Syllabus',
      );
      service['courses'] = [existingCourse];
      service.update = jest.fn().mockResolvedValue(undefined);
      await controller.update(1, updateCourseDto);
      expect(service.update).toHaveBeenCalledWith(1, updateCourseDto);
    });
    it('should throw NotFoundException if course not found for update', async () => {
      const updateCourseDto = new UpdateCourseDto(
        'Updated Course',
        'Updated Syllabus',
      );
      await expect(controller.update(999, updateCourseDto)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
  describe('remove()', () => {
    it('should remove a course', async () => {
      const course = new CourseEntity(1, 'Course 1', 'Syllabus 1');
      service['courses'] = [course];
      service.remove = jest.fn().mockResolvedValue(undefined);
      await controller.remove(1);
      expect(service.remove).toHaveBeenCalledWith(1);
    });
    it('should throw NotFoundException if course not found for removal', async () => {
      await expect(controller.remove(999)).rejects.toThrow(NotFoundException);
    });
  });
});
