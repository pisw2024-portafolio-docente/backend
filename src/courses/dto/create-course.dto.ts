import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateCourseDto {
  @IsString()
  @IsNotEmpty()
  @Length(4, 64)
  private name: string;
  @IsString()
  @IsNotEmpty()
  @Length(4, 256)
  private syllabus: string;
  constructor(name: string, syllabus: string) {
    this.name = name;
    this.syllabus = syllabus;
  }
  getName(): string {
    return this.name;
  }
  getSyllabus(): string {
    return this.syllabus;
  }
}
