import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { CourseRepository } from './persistence/course.repository';
import * as Papa from 'papaparse';

@Injectable()
export class CoursesService {
  constructor(private readonly courseRepository: CourseRepository) {}
  findAll() {
    return this.courseRepository.getAllCourses();
  }
  async findOne(id: number) {
    const course = await this.courseRepository.getCourseById(id);
    if (!course) {
      throw new NotFoundException(`Course with id = ${id} does not exists!`);
    }
    return course;
  }
  create(createCourseDto: CreateCourseDto) {
    return this.courseRepository.createCourse(createCourseDto);
  }
  createFromCsv(csvData: string) {
    const coursesParser = Papa.parse<CreateCourseDto>(csvData, {
      header: true,
      dynamicTyping: true,
      delimiter: ',',
      skipEmptyLines: true,
      complete: (results, file) => {
        if (results.errors.length) {
          console.error('Error while parsing', file, results.errors);
        }
      },
    });
    const courses = coursesParser.data;
    return this.courseRepository.createCourses(courses);
  }
  async update(id: number, updateCourseDto: UpdateCourseDto) {
    const course = await this.courseRepository.getCourseById(id);
    if (!course) {
      throw new NotFoundException(`Course with id = ${id} does not exists!`);
    }
    this.courseRepository.updateCourse(id, updateCourseDto);
  }
  async remove(id: number) {
    const course = await this.courseRepository.getCourseById(id);
    if (!course) {
      throw new NotFoundException(`Course with id = ${id} does not exists!`);
    }
    this.courseRepository.deleteCourse(id);
  }
}
