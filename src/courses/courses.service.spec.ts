import { Test, TestingModule } from '@nestjs/testing';
import { CoursesService } from './courses.service';
import { CourseRepository } from './persistence/course.repository';
import { CourseRepositoryMock } from '../../__mocks__/courses/course.repository.mock';
import { CourseEntity } from './persistence/course.entity';
import { NotFoundException } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

describe('CoursesService', () => {
  let coursesService: CoursesService;
  let courseRepositoryMock: CourseRepositoryMock;
  beforeEach(async () => {
    courseRepositoryMock = new CourseRepositoryMock();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CoursesService,
        {
          provide: CourseRepository,
          useValue: courseRepositoryMock,
        },
      ],
    }).compile();
    coursesService = module.get<CoursesService>(CoursesService);
  });
  it('should be defined', () => {
    expect(coursesService).toBeDefined();
  });
  describe('findAll()', () => {
    it('should return an array of courses', async () => {
      const course1 = new CourseEntity(1, 'Course 1', 'Syllabus 1');
      const course2 = new CourseEntity(2, 'Course 2', 'Syllabus 2');
      courseRepositoryMock.courses = [course1, course2];
      const result = await coursesService.findAll();
      expect(result).toEqual([course1, course2]);
    });
  });
  describe('findOne()', () => {
    it('should return a course by id', async () => {
      const id = 1;
      const course = new CourseEntity(id, 'Course 1', 'Syllabus 1');
      courseRepositoryMock.courses = [course];
      const result = await coursesService.findOne(id);
      expect(result).toEqual(course);
    });
    it('should throw NotFoundException if course not found', async () => {
      const id = 999;
      jest.spyOn(courseRepositoryMock, 'getCourseById').mockResolvedValue(null);
      await expect(coursesService.findOne(id)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
  describe('create()', () => {
    it('should create a new course', async () => {
      const createCourseDto = new CreateCourseDto('Course 3', 'Syllabus 3');
      const result = await coursesService.create(createCourseDto);
      expect(result).toBeInstanceOf(CourseEntity);
    });
  });
  describe('update()', () => {
    it('should update a course by id', async () => {
      const id = 1;
      const existingCourse = new CourseEntity(id, 'Old Course', 'Old Syllabus');
      const updateCourseDto = new UpdateCourseDto(
        'Updated Course',
        'Updated Syllabus',
      );
      courseRepositoryMock.courses = [existingCourse];
      await coursesService.update(id, updateCourseDto);
      const updatedCourse = courseRepositoryMock.courses.find(
        (course) => course.getId() === id,
      );
      expect(updatedCourse).toBeDefined();
      expect(updatedCourse.getName()).toBe(updateCourseDto.getName());
      expect(updatedCourse.getSyllabus()).toBe(updateCourseDto.getSyllabus());
    });
    it('should throw NotFoundException if course not found', async () => {
      const id = 999;
      const updateCourseDto = new UpdateCourseDto(
        'Updated Course',
        'Updated Syllabus',
      );
      courseRepositoryMock.courses = [];
      await expect(coursesService.update(id, updateCourseDto)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
  describe('remove()', () => {
    it('should delete a course by id', async () => {
      const id = 1;
      const existingCourse = new CourseEntity(
        id,
        'Course to Delete',
        'Syllabus',
      );
      courseRepositoryMock.courses = [existingCourse];
      await coursesService.remove(id);
      const deletedCourse = courseRepositoryMock.courses.find(
        (course) => course.getId() === id,
      );
      expect(deletedCourse).toBeUndefined();
    });
    it('should throw NotFoundException if course not found', async () => {
      const id = 999;
      courseRepositoryMock.courses = [];
      await expect(coursesService.remove(id)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
});
