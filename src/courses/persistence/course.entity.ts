import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('courses')
export class CourseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'varchar', length: 64 })
  private name: string;
  @Column({ type: 'varchar', length: 256 })
  private syllabus: string;
  @CreateDateColumn()
  private createdAt: Date;
  constructor(id: number, name: string, syllabus: string) {
    this.id = id;
    this.name = name;
    this.syllabus = syllabus;
    this.createdAt = new Date();
  }
  getId(): number {
    return this.id;
  }
  getName(): string {
    return this.name;
  }
  getSyllabus(): string {
    return this.syllabus;
  }
  getCreatedAt(): Date {
    return this.createdAt;
  }
}
