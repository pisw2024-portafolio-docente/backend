import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CourseEntity } from './course.entity';
import { Repository } from 'typeorm';
import { CreateCourseDto } from '../dto/create-course.dto';
import { UpdateCourseDto } from '../dto/update-course.dto';

@Injectable()
export class CourseRepository {
  constructor(
    @InjectRepository(CourseEntity)
    private readonly _courseRepository: Repository<CourseEntity>,
  ) {}
  async getAllCourses() {
    return this._courseRepository.find();
  }
  async getCourseById(id: number) {
    return this._courseRepository.findOneBy({ id: id });
  }
  async createCourse(createCourseDto: CreateCourseDto) {
    this._courseRepository.save(createCourseDto);
  }
  async createCourses(createCourseDtos: CreateCourseDto[]) {
    this._courseRepository.save(createCourseDtos);
  }
  async updateCourse(id: number, updateCourseDto: UpdateCourseDto) {
    this._courseRepository.update(id, updateCourseDto);
  }
  async deleteCourse(id: number) {
    this._courseRepository.delete(id);
  }
}
