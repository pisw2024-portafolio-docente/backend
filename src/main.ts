import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  if (configService.get<string>('APP_ENV').toLowerCase() !== 'production') {
    app.enableCors();
  }
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
  const port = configService.get<number>('APP_PORT') || 3000;
  const documentConfig = new DocumentBuilder()
    .setTitle('Professor Portfolio')
    .setDescription('Web Services with NestJs 10')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const swaggerDocument = SwaggerModule.createDocument(app, documentConfig);
  SwaggerModule.setup('/api/v1/docs/', app, swaggerDocument);
  await app.listen(port);
}
bootstrap();
