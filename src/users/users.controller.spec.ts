import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UsersServiceMock } from '../../__mocks__/users/users.service.mock';
import { CreateUserDto } from './dto/create-user.dto';
import { NotFoundException } from '@nestjs/common';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersServiceMock;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useClass: UsersServiceMock,
        },
      ],
    }).compile();
    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService) as unknown as UsersServiceMock;
  });
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findAll', () => {
    it('should return an array of users', async () => {
      const result = [
        {
          id: '1',
          username: 'user1',
          firstName: 'First',
          lastName: 'Last',
          email: 'user1@example.com',
          enabled: true,
        },
        {
          id: '2',
          username: 'user2',
          firstName: 'Second',
          lastName: 'Last',
          email: 'user2@example.com',
          enabled: true,
        },
      ];
      jest.spyOn(service, 'findAll').mockImplementation(async () => result);
      expect(await controller.findAll()).toEqual(result);
    });
  });
  describe('findOne', () => {
    it('should return a user by id', async () => {
      const result = {
        id: '1',
        username: 'user1',
        firstName: 'First',
        lastName: 'Last',
        email: 'user1@example.com',
        enabled: true,
      };
      jest.spyOn(service, 'findOne').mockImplementation(async () => result);
      expect(await controller.findOne('1')).toEqual(result);
    });
    it('should throw NotFoundException if user not found', async () => {
      jest.spyOn(service, 'findOne').mockImplementation(async () => {
        throw new NotFoundException('User with id = 1 does not exist!');
      });
      await expect(controller.findOne('1')).rejects.toThrow(NotFoundException);
    });
  });
  describe('create', () => {
    it('should create a user and return the id', async () => {
      const createUserDto = new CreateUserDto('user2', 'Second', 'Last', 'user2@example.com', 'password');
      const result = '2';
      jest.spyOn(service, 'create').mockImplementation(async () => result);
      expect(await controller.create(createUserDto)).toEqual(result);
    });
  });
});
