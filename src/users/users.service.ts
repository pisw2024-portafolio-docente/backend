import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { KeycloakAdminConfigService } from '../../config/keycloak/keycloak.admin.config.service';
import { Credentials } from '@keycloak/keycloak-admin-client/lib/utils/auth';
import KcAdminClient from '@keycloak/keycloak-admin-client';
import UserRepresentation from '@keycloak/keycloak-admin-client/lib/defs/userRepresentation';

@Injectable()
export class UsersService {
  private kcAdminClient: KcAdminClient;
  private readonly credentials: Credentials;
  constructor(
    private readonly keycloakAdminConfigService: KeycloakAdminConfigService,
  ) {
    setTimeout(async () => {
      this.initializeAdminClient();
    }, 100);
    this.credentials = {
      username: this.keycloakAdminConfigService.masterUsername,
      password: this.keycloakAdminConfigService.masterPassword,
      grantType: 'password',
      clientId: 'admin-cli',
    };
  }
  async findAll() {
    try {
      await this.kcAdminClient.auth(this.credentials);
      this.changeToApplicationRealm();
      const users = await this.kcAdminClient.users.find();
      this.changeToMasterRealm();
      const mappedUsers = users.map(this.mapKeycloakUserToAppUser);
      return mappedUsers;
    } catch (error) {
      console.log(error);
    }
  }
  async findOne(id: string) {
    try {
      await this.kcAdminClient.auth(this.credentials);
      this.changeToApplicationRealm();
      const user = await this.kcAdminClient.users.findOne({
        id,
        userProfileMetadata: false,
      });
      this.changeToMasterRealm();
      if (!user) {
        throw new NotFoundException(`User with id = ${id} does not exists!`);
      }
      return this.mapKeycloakUserToAppUser(user);
    } catch (error) {
      console.log(error);
    }
  }
  async create(createUserDto: CreateUserDto) {
    try {
      await this.kcAdminClient.auth(this.credentials);
      this.changeToApplicationRealm();
      const id = await this.kcAdminClient.users.create({
        realm: this.keycloakAdminConfigService.applicationRealm,
        username: createUserDto.username,
        firstName: createUserDto.firstName,
        lastName: createUserDto.lastName,
        email: createUserDto.email,
        emailVerified: true,
        credentials: [
          {
            type: 'password',
            value: createUserDto.password,
            temporary: false,
          },
        ],
        enabled: true,
      });
      this.changeToMasterRealm();
      return id;
    } catch (error) {
      console.log(error);
    }
  }
  private dynamicKeycloakImport = async () =>
    new Function("return import('@keycloak/keycloak-admin-client')")();
  private async initializeAdminClient() {
    try {
      this.kcAdminClient = new (await this.dynamicKeycloakImport()).default();
      this.kcAdminClient.setConfig({
        baseUrl: this.keycloakAdminConfigService.baseUrl,
        realmName: this.keycloakAdminConfigService.masterRealm,
      });
      await this.kcAdminClient.auth(this.credentials);
    } catch (error) {
      console.log(error);
    }
  }
  private changeToMasterRealm() {
    this.kcAdminClient.setConfig({
      realmName: this.keycloakAdminConfigService.masterRealm,
    });
  }
  private changeToApplicationRealm() {
    this.kcAdminClient.setConfig({
      realmName: this.keycloakAdminConfigService.applicationRealm,
    });
  }
  private mapKeycloakUserToAppUser(userRepresentation: UserRepresentation) {
    return {
      id: userRepresentation.id,
      username: userRepresentation.username,
      firstName: userRepresentation.firstName,
      lastName: userRepresentation.lastName,
      email: userRepresentation.email,
      enabled: userRepresentation.enabled,
      roles: userRepresentation.realmRoles,
    };
  }
}
