import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { KeycloakConfigModule } from '../../config/keycloak/keycloak.config.module';

@Module({
  imports: [KeycloakConfigModule],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
