import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  @Length(4, 32)
  username: string;
  @IsString()
  @IsNotEmpty()
  @Length(4, 32)
  firstName: string;
  @IsString()
  @IsNotEmpty()
  @Length(4, 32)
  lastName: string;
  @IsEmail()
  @IsNotEmpty()
  @Length(4, 64)
  email: string;
  @IsString()
  @IsNotEmpty()
  password: string;
  constructor(username: string, firstName: string, lastName: string, email: string, password: string) {
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  }
}
