import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RoleMatchingMode, Roles } from 'nest-keycloak-connect';

@ApiTags('Users')
@ApiBearerAuth()
@Controller('/api/v1/users')
@Roles({ roles: ['admin'], mode: RoleMatchingMode.ALL })
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  @Get()
  findAll() {
    return this.usersService.findAll();
  }
  @Get(':id')
  findOne(@Param('id') username: string) {
    return this.usersService.findOne(username);
  }
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }
}
