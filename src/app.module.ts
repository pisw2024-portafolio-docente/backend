import { Module } from '@nestjs/common';
import {
  AuthGuard,
  KeycloakConnectModule,
  ResourceGuard,
  RoleGuard,
} from 'nest-keycloak-connect';
import { APP_GUARD } from '@nestjs/core';
import { DatabaseConfigModule } from 'config/database/database.config.module';
import { KeycloakConfigService } from 'config/keycloak/keycloak.config.service';
import { KeycloakConfigModule } from 'config/keycloak/keycloak.config.module';
import { SemestersModule } from './semesters/semesters.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigModule } from 'config/orm/type.orm.config.module';
import { TypeOrmConfigService } from 'config/orm/type.orm.config.service';
import { UsersModule } from './users/users.module';
import { PortfoliosModule } from './portfolios/portfolios.module';
import { CoursesModule } from './courses/courses.module';
import { AppConfigModule } from 'config/app/app.config.module';

@Module({
  imports: [
    AppConfigModule,
    DatabaseConfigModule,
    KeycloakConnectModule.registerAsync({
      useExisting: KeycloakConfigService,
      imports: [KeycloakConfigModule],
    }),
    TypeOrmModule.forRootAsync({
      useExisting: TypeOrmConfigService,
      imports: [TypeOrmConfigModule],
    }),
    SemestersModule,
    PortfoliosModule,
    UsersModule,
    CoursesModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: ResourceGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
})
export class AppModule {}
