import { Test, TestingModule } from '@nestjs/testing';
import { PortfoliosController } from './portfolios.controller';
import { PortfoliosService } from './portfolios.service';
import { PortfoliosServiceMock } from '../../__mocks__/portfolios/portfolios.service.mock';
import { PortfolioEntity } from './persistence/portfolio.entity';
import { NotFoundException } from '@nestjs/common';
import { CreatePortfolioDto } from './dto/create-portfolio.dto';
import { UpdatePortfolioDto } from './dto/update-portfolio.dto';

describe('PortfoliosController', () => {
  let controller: PortfoliosController;
  let service: PortfoliosServiceMock;
  beforeEach(async () => {
    service = new PortfoliosServiceMock();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PortfoliosController],
      providers: [
        {
          provide: PortfoliosService,
          useValue: service,
        },
      ],
    }).compile();
    controller = module.get<PortfoliosController>(PortfoliosController);
  });
  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findAll()', () => {
    it('should return an array of portfolios', async () => {
      const result = [
        new PortfolioEntity(1, 'Course1', 'Portfolio1', 30),
        new PortfolioEntity(2, 'Course2', 'Portfolio2', 20),
      ];
      jest.spyOn(service, 'findAll').mockResolvedValue(result);
      expect(await controller.findAll()).toEqual(result);
    });
  });
  describe('findOne()', () => {
    it('should return a portfolio by id', async () => {
      const result = new PortfolioEntity(1, 'Course1', 'Portfolio1', 30);
      jest.spyOn(service, 'findOne').mockResolvedValue(result);
      expect(await controller.findOne(1)).toEqual(result);
    });
    it('should throw NotFoundException if portfolio is not found', async () => {
      jest
        .spyOn(service, 'findOne')
        .mockRejectedValue(
          new NotFoundException('Portfolio with id = 1 does not exist!'),
        );
      await expect(controller.findOne(1)).rejects.toThrow(NotFoundException);
    });
  });
  describe('create()', () => {
    it('should create a new portfolio', async () => {
      const createPortfolioDto = new CreatePortfolioDto(
        'Course1',
        'Portfolio1',
        30,
      );
      const result = new PortfolioEntity(3, 'Course1', 'Portfolio1', 30);
      jest.spyOn(service, 'create').mockResolvedValue(result);
      expect(await controller.create(createPortfolioDto)).toEqual(result);
    });
  });
  describe('update()', () => {
    it('should update an existing portfolio', async () => {
      const updatePortfolioDto = new UpdatePortfolioDto(
        'Course1',
        'Updated Portfolio',
        40,
      );
      const updatedPortfolio = new PortfolioEntity(
        1,
        'Course1',
        'Updated Portfolio',
        40,
      );
      jest.spyOn(service, 'update').mockResolvedValue(updatedPortfolio);
      expect(await controller.update(1, updatePortfolioDto)).toEqual(
        updatedPortfolio,
      );
    });
    it('should throw NotFoundException if portfolio is not found', async () => {
      jest
        .spyOn(service, 'update')
        .mockRejectedValue(
          new NotFoundException('Portfolio with id = 1 does not exist!'),
        );
      await expect(
        controller.update(
          1,
          new UpdatePortfolioDto('Course1', 'Updated Portfolio', 40),
        ),
      ).rejects.toThrow(NotFoundException);
    });
  });
  describe('remove()', () => {
    it('should remove an existing portfolio', async () => {
      expect(service.portfolios.length).toBe(2);
      jest.spyOn(service, 'remove').mockImplementation(async (id: number) => {
        service.portfolios = service.portfolios.filter((p) => p.getId() !== id);
      });
      await controller.remove(1);
      expect(service.portfolios.length).toBe(1);
    });
    it('should throw NotFoundException if portfolio is not found', async () => {
      jest
        .spyOn(service, 'remove')
        .mockRejectedValue(
          new NotFoundException('Portfolio with id = 1 does not exist!'),
        );
      await expect(controller.remove(1)).rejects.toThrow(NotFoundException);
    });
  });
});
