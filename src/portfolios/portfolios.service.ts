import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePortfolioDto } from './dto/create-portfolio.dto';
import { UpdatePortfolioDto } from './dto/update-portfolio.dto';
import { PortfolioRepository } from './persistence/portfolio.repository';

@Injectable()
export class PortfoliosService {
  constructor(private readonly portfolioRepository: PortfolioRepository) {}
  findAll() {
    return this.portfolioRepository.getAllPortfolios();
  }
  async findOne(id: number) {
    const portfolio = await this.portfolioRepository.getPortfolioById(id);
    if (!portfolio) {
      throw new NotFoundException(`Portfolio with id = ${id} does not exists!`);
    }
    return portfolio;
  }
  create(createPortfolioDto: CreatePortfolioDto) {
    return this.portfolioRepository.createPortfolio(createPortfolioDto);
  }
  async update(id: number, updatePortfolioDto: UpdatePortfolioDto) {
    const portfolio = await this.portfolioRepository.getPortfolioById(id);
    if (!portfolio) {
      throw new NotFoundException(`Portfolio with id = ${id} does not exists!`);
    }
    return this.portfolioRepository.updatePortfolio(id, updatePortfolioDto);
  }
  async remove(id: number) {
    const portfolio = await this.portfolioRepository.getPortfolioById(id);
    if (!portfolio) {
      throw new NotFoundException(`Portfolio with id = ${id} does not exists!`);
    }
    return this.portfolioRepository.deletePortfolioById(id);
  }
}
