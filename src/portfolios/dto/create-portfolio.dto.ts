import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class CreatePortfolioDto {
  @IsString()
  @IsNotEmpty()
  private course: string;
  @IsString()
  @IsNotEmpty()
  private name: string;
  @IsInt()
  @IsNotEmpty()
  private studentsAmount: number;
  constructor(course: string, name: string, studentsAmount: number) {
    this.course = course;
    this.name = name;
    this.studentsAmount = studentsAmount;
  }
  getCourse() {
    return this.course;
  }
  getName() {
    return this.name;
  }
  getStudentsAmmount() {
    return this.studentsAmount;
  }
}
