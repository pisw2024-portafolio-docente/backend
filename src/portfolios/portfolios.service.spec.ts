import { Test, TestingModule } from '@nestjs/testing';
import { PortfoliosService } from './portfolios.service';
import { PortfolioRepository } from './persistence/portfolio.repository';
import { PortfolioRepositoryMock } from '../../__mocks__/portfolios/portfolio.repository.mock';
import { PortfolioEntity } from './persistence/portfolio.entity';
import { CreatePortfolioDto } from './dto/create-portfolio.dto';
import { NotFoundException } from '@nestjs/common';
import { UpdatePortfolioDto } from './dto/update-portfolio.dto';

describe('PortfoliosService', () => {
  let portfoliosService: PortfoliosService;
  let portfolioRepositoryMock: PortfolioRepositoryMock;
  beforeEach(async () => {
    portfolioRepositoryMock = new PortfolioRepositoryMock();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PortfoliosService,
        {
          provide: PortfolioRepository,
          useValue: portfolioRepositoryMock,
        },
      ],
    }).compile();
    portfoliosService = module.get<PortfoliosService>(PortfoliosService);
  });
  it('should be defined', () => {
    expect(portfoliosService).toBeDefined();
  });
  describe('findAll()', () => {
    it('should return an array of portfolios', async () => {
      const portfolio1 = new PortfolioEntity(1, 'Course1', 'Portfolio1', 30);
      const portfolio2 = new PortfolioEntity(2, 'Course2', 'Portfolio2', 20);
      portfolioRepositoryMock.portfolios = [portfolio1, portfolio2];
      expect(await portfoliosService.findAll()).toEqual([
        portfolio1,
        portfolio2,
      ]);
    });
  });
  describe('findOne()', () => {
    it('should return a portfolio by id', async () => {
      const portfolio = new PortfolioEntity(1, 'Course1', 'Portfolio1', 30);
      jest
        .spyOn(portfolioRepositoryMock, 'getPortfolioById')
        .mockResolvedValue(portfolio);
      expect(await portfoliosService.findOne(1)).toEqual(portfolio);
    });
    it('should throw NotFoundException if portfolio is not found', async () => {
      jest
        .spyOn(portfolioRepositoryMock, 'getPortfolioById')
        .mockResolvedValue(null);
      await expect(portfoliosService.findOne(1)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
  describe('create()', () => {
    it('should create a new portfolio', async () => {
      const createPortfolioDto = new CreatePortfolioDto(
        'Course1',
        'Portfolio1',
        30,
      );
      const portfolio = new PortfolioEntity(1, 'Course1', 'Portfolio1', 30);
      jest
        .spyOn(portfolioRepositoryMock, 'createPortfolio')
        .mockResolvedValue(undefined);
      portfolioRepositoryMock.portfolios.push(portfolio);
      await portfoliosService.create(createPortfolioDto);
      expect(portfolioRepositoryMock.portfolios).toContainEqual(portfolio);
    });
  });
  describe('update()', () => {
    it('should update an existing portfolio', async () => {
      const updatePortfolioDto = new UpdatePortfolioDto(
        'Course1',
        'Updated Portfolio',
        40,
      );
      const existingPortfolio = new PortfolioEntity(
        1,
        'Course1',
        'Old Portfolio',
        30,
      );
      const updatedPortfolio = new PortfolioEntity(
        1,
        'Course1',
        'Updated Portfolio',
        40,
      );
      portfolioRepositoryMock.portfolios.push(existingPortfolio);
      jest
        .spyOn(portfolioRepositoryMock, 'getPortfolioById')
        .mockResolvedValue(existingPortfolio);
      jest
        .spyOn(portfolioRepositoryMock, 'updatePortfolio')
        .mockImplementation(async (id: number, dto: UpdatePortfolioDto) => {
          const index = portfolioRepositoryMock.portfolios.findIndex(
            (p) => p.getId() === id,
          );
          if (index !== -1) {
            portfolioRepositoryMock.portfolios[index] = new PortfolioEntity(
              id,
              dto.getCourse(),
              dto.getName(),
              dto.getStudentsAmmount(),
            );
          }
        });
      await portfoliosService.update(1, updatePortfolioDto);
      const updated = portfolioRepositoryMock.portfolios.find(
        (p) => p.getId() === 1,
      );
      expect(updated).toEqual(updatedPortfolio);
    });
    it('should throw NotFoundException if portfolio is not found', async () => {
      const updatePortfolioDto = new UpdatePortfolioDto(
        'Course1',
        'Updated Portfolio',
        40,
      );
      jest
        .spyOn(portfolioRepositoryMock, 'getPortfolioById')
        .mockResolvedValue(null);
      await expect(
        portfoliosService.update(1, updatePortfolioDto),
      ).rejects.toThrow(NotFoundException);
    });
  });
  describe('remove()', () => {
    it('should remove an existing portfolio', async () => {
      const portfolio = new PortfolioEntity(1, 'Course1', 'Portfolio1', 30);
      portfolioRepositoryMock.portfolios.push(portfolio);
      jest
        .spyOn(portfolioRepositoryMock, 'getPortfolioById')
        .mockResolvedValue(portfolio);
      jest
        .spyOn(portfolioRepositoryMock, 'deletePortfolioById')
        .mockImplementation(async (id: number) => {
          const index = portfolioRepositoryMock.portfolios.findIndex(
            (p) => p.getId() === id,
          );
          if (index !== -1) {
            portfolioRepositoryMock.portfolios.splice(index, 1);
          }
        });
      await portfoliosService.remove(1);
      expect(portfolioRepositoryMock.portfolios.length).toBe(0);
    });
    it('should throw NotFoundException if portfolio is not found', async () => {
      jest
        .spyOn(portfolioRepositoryMock, 'getPortfolioById')
        .mockResolvedValue(null);
      await expect(portfoliosService.remove(1)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
});
