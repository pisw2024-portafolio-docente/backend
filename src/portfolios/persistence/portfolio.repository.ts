import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PortfolioEntity } from './portfolio.entity';
import { Repository } from 'typeorm';
import { CreatePortfolioDto } from '../dto/create-portfolio.dto';
import { UpdatePortfolioDto } from '../dto/update-portfolio.dto';

@Injectable()
export class PortfolioRepository {
  constructor(
    @InjectRepository(PortfolioEntity)
    private readonly _portfolioRepository: Repository<PortfolioEntity>,
  ) {}
  async getAllPortfolios(): Promise<PortfolioEntity[]> {
    return this._portfolioRepository.find();
  }
  async getPortfolioById(id: number): Promise<PortfolioEntity> {
    return this._portfolioRepository.findOneBy({ id: id });
  }
  async createPortfolio(createPortfolioDto: CreatePortfolioDto): Promise<void> {
    this._portfolioRepository.save(createPortfolioDto);
  }
  async updatePortfolio(
    id: number,
    updatePortfolioDto: UpdatePortfolioDto,
  ): Promise<void> {
    this._portfolioRepository.update(id, updatePortfolioDto);
  }
  async deletePortfolioById(id: number): Promise<void> {
    this._portfolioRepository.delete(id);
  }
}
