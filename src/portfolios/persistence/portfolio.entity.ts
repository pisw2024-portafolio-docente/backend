import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('portfolios')
export class PortfolioEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'varchar', length: 64 })
  private course: string;
  @Column({ type: 'varchar', length: 64 })
  private name: string;
  @Column({ type: 'integer' })
  private studentsAmount: number;
  @CreateDateColumn()
  private createdAt: Date;
  constructor(
    id: number,
    course: string,
    name: string,
    studentsAmount: number,
  ) {
    this.id = id;
    this.course = course;
    this.name = name;
    this.studentsAmount = studentsAmount;
    this.createdAt = new Date();
  }
  getId(): number {
    return this.id;
  }
  getCourse(): string {
    return this.course;
  }
  getName(): string {
    return this.name;
  }
  getStudentsAmount(): number {
    return this.studentsAmount;
  }
  getCreatedAt(): Date {
    return this.createdAt;
  }
}
