import { Module } from '@nestjs/common';
import { PortfoliosService } from './portfolios.service';
import { PortfoliosController } from './portfolios.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PortfolioEntity } from './persistence/portfolio.entity';
import { PortfolioRepository } from './persistence/portfolio.repository';

@Module({
  imports: [TypeOrmModule.forFeature([PortfolioEntity])],
  controllers: [PortfoliosController],
  providers: [PortfolioRepository, PortfoliosService],
})
export class PortfoliosModule {}
