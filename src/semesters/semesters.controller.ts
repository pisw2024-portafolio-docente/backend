import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
} from '@nestjs/common';
import { SemestersService } from './semesters.service';
import { CreateSemesterDto } from './dto/create-semester.dto';
import { UpdateSemesterDto } from './dto/update-semester.dto';
import { RoleMatchingMode, Roles } from 'nest-keycloak-connect';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('Semesters')
@ApiBearerAuth()
@Controller('/api/v1/semesters')
@Roles({ roles: ['admin'], mode: RoleMatchingMode.ALL })
export class SemestersController {
  constructor(private readonly semestersService: SemestersService) {}
  @Get()
  findAll() {
    return this.semestersService.findAll();
  }
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.semestersService.findOne(id);
  }
  @Post()
  create(@Body() createSemesterDto: CreateSemesterDto) {
    return this.semestersService.createOne(createSemesterDto);
  }
  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateSemesterDto: UpdateSemesterDto,
  ) {
    return this.semestersService.update(id, updateSemesterDto);
  }
  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.semestersService.remove(id);
  }
}
