import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('semesters')
export class SemesterEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ type: 'varchar', length: 100 })
  private name: string;
  @Column({ type: 'integer' })
  private year: number;
  @Column({ type: 'date' })
  private startAt: Date;
  @Column({ type: 'date' })
  private endsAt: Date;
  @CreateDateColumn()
  private createdAt: Date;
  constructor(
    id: number,
    name: string,
    year: number,
    startAt: Date,
    endsAt: Date,
  ) {
    this.id = id;
    this.name = name;
    this.year = year;
    this.startAt = startAt;
    this.endsAt = endsAt;
    this.createdAt = new Date();
  }
  getId(): number {
    return this.id;
  }
  getName(): string {
    return this.name;
  }
  getYear(): number {
    return this.year;
  }
  getStartAt(): Date {
    return this.startAt;
  }
  getEndsAt(): Date {
    return this.endsAt;
  }
  getCreatedAt(): Date {
    return this.createdAt;
  }
}
