import { InjectRepository } from '@nestjs/typeorm';
import { SemesterEntity } from './semester.entity';
import { Repository } from 'typeorm';
import { CreateSemesterDto } from '../dto/create-semester.dto';
import { UpdateSemesterDto } from '../dto/update-semester.dto';

export class SemesterRepository {
  constructor(
    @InjectRepository(SemesterEntity)
    private readonly _semesterRepository: Repository<SemesterEntity>,
  ) {}
  async getAllSemesters(): Promise<SemesterEntity[]> {
    return this._semesterRepository.find();
  }
  async getSemesterById(id: number): Promise<SemesterEntity> {
    return this._semesterRepository.findOneBy({ id: id });
  }
  async createSemester(createSemesterDto: CreateSemesterDto): Promise<void> {
    this._semesterRepository.save(createSemesterDto);
  }
  async updateSemester(
    id: number,
    updateSemesterDto: UpdateSemesterDto,
  ): Promise<void> {
    this._semesterRepository.update(id, updateSemesterDto);
  }
  async deleteSemesterById(id: number) {
    this._semesterRepository.delete(id);
  }
}
