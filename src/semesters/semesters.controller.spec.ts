import { Test, TestingModule } from '@nestjs/testing';
import { SemestersController } from './semesters.controller';
import { SemestersService } from './semesters.service';
import { SemestersServiceMock } from '../../__mocks__/semesters/semesters.service.mock';
import { SemesterEntity } from './persistence/semester.entity';
import { NotFoundException } from '@nestjs/common';
import { CreateSemesterDto } from './dto/create-semester.dto';
import { UpdateSemesterDto } from './dto/update-semester.dto';

describe('SemestersController', () => {
  let controller: SemestersController;
  let semestersService: SemestersServiceMock;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SemestersController],
      providers: [
        {
          provide: SemestersService,
          useClass: SemestersServiceMock,
        },
      ],
    }).compile();
    controller = module.get<SemestersController>(SemestersController);
    semestersService = module.get<SemestersService>(SemestersService) as unknown as SemestersServiceMock;
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('findAll()', () => {
    it('should return an array of semesters', async () => {
      const result: SemesterEntity[] = [];
      jest.spyOn(semestersService, 'findAll').mockResolvedValue(result);

      expect(await controller.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return a single semester by id', async () => {
      const id = 1;
      const result = new SemesterEntity(id, 'Semester 1', 2024, new Date(), new Date());
      jest.spyOn(semestersService, 'findOne').mockResolvedValue(result);

      expect(await controller.findOne(id)).toBe(result);
    });
    it('should throw NotFoundException if the semester is not found', async () => {
      const id = 999;
      jest.spyOn(semestersService, 'findOne').mockRejectedValue(new NotFoundException(`Semester with id = ${id} does not exists!`));
      await expect(controller.findOne(id)).rejects.toThrow(NotFoundException);
    });
  });
  describe('create', () => {
    it('should create a new semester', async () => {
      const createSemesterDto = new CreateSemesterDto('Semester 1', 2024, new Date(), new Date());
      jest.spyOn(semestersService, 'createOne').mockResolvedValue(undefined);
      await expect(controller.create(createSemesterDto)).resolves.not.toThrow();
    });
  });
  describe('update', () => {
    it('should update an existing semester', async () => {
      const id = 1;
      const updateSemesterDto = new UpdateSemesterDto('Updated Semester', 2024, new Date(), new Date());
      jest.spyOn(semestersService, 'update').mockResolvedValue(undefined);
      await expect(controller.update(id, updateSemesterDto)).resolves.not.toThrow();
    });
    it('should throw NotFoundException if the semester to update is not found', async () => {
      const id = 999;
      const updateSemesterDto = new UpdateSemesterDto('Updated Semester', 2024, new Date(), new Date());
      jest.spyOn(semestersService, 'update').mockRejectedValue(new NotFoundException(`Semester with id = ${id} does not exists!`));
      await expect(controller.update(id, updateSemesterDto)).rejects.toThrow(NotFoundException);
    });
  });
  describe('remove', () => {
    it('should remove an existing semester', async () => {
      const id = 1;
      jest.spyOn(semestersService, 'remove').mockResolvedValue(undefined);
      await expect(controller.remove(id)).resolves.not.toThrow();
    });
    it('should throw NotFoundException if the semester to remove is not found', async () => {
      const id = 999;
      jest.spyOn(semestersService, 'remove').mockRejectedValue(new NotFoundException(`Semester with id = ${id} does not exists!`));
      await expect(controller.remove(id)).rejects.toThrow(NotFoundException);
    });
  });
});
