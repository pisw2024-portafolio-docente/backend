import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSemesterDto } from './dto/create-semester.dto';
import { UpdateSemesterDto } from './dto/update-semester.dto';
import { SemesterRepository } from './persistence/semester.repository';

@Injectable()
export class SemestersService {
  constructor(private readonly semesterRepository: SemesterRepository) {}
  async findAll() {
    return this.semesterRepository.getAllSemesters();
  }
  async findOne(id: number) {
    const semester = await this.semesterRepository.getSemesterById(id);
    if (!semester) {
      throw new NotFoundException(`Semester with id = ${id} does not exists!`);
    }
    return semester;
  }
  async createOne(createSemesterDto: CreateSemesterDto) {
    return this.semesterRepository.createSemester(createSemesterDto);
  }
  async update(id: number, updateSemesterDto: UpdateSemesterDto) {
    const semester = await this.semesterRepository.getSemesterById(id);
    if (!semester) {
      throw new NotFoundException(`Semester with id = ${id} does not exists!`);
    }
    return this.semesterRepository.updateSemester(id, updateSemesterDto);
  }
  async remove(id: number) {
    const semester = await this.semesterRepository.getSemesterById(id);
    if (!semester) {
      throw new NotFoundException(`Semester with id = ${id} does not exists!`);
    }
    return this.semesterRepository.deleteSemesterById(id);
  }
}
