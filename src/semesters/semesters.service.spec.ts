import { Test, TestingModule } from '@nestjs/testing';
import { SemestersService } from './semesters.service';
import { SemesterRepositoryMock } from '../../__mocks__/semesters/semester.repository.mock';
import { SemesterEntity } from './persistence/semester.entity';
import { CreateSemesterDto } from './dto/create-semester.dto';
import { UpdateSemesterDto } from './dto/update-semester.dto';
import { SemesterRepository } from './persistence/semester.repository';
import { NotFoundException } from '@nestjs/common';

describe('SemestersService', () => {
  let semestersService: SemestersService;
  let semesterRepositoryMock: SemesterRepositoryMock;
  beforeEach(async () => {
    semesterRepositoryMock = new SemesterRepositoryMock();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SemestersService,
        {
          provide: SemesterRepository,
          useValue: semesterRepositoryMock,
        },
      ],
    }).compile();
    semestersService = module.get<SemestersService>(SemestersService);
  });
  it('should be defined', () => {
    expect(semestersService).toBeDefined();
  });
  describe('findAll', () => {
    it('should return an array of semesters', async () => {
      const semester1 = new SemesterEntity(
        1,
        'Semester 1',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      const semester2 = new SemesterEntity(
        2,
        'Semester 2',
        2024,
        new Date('2024-07-01'),
        new Date('2024-12-31'),
      );
      semesterRepositoryMock['semesters'] = [semester1, semester2];
      const result = await semestersService.findAll();
      expect(result).toEqual([semester1, semester2]);
    });
  });
  describe('findOne', () => {
    it('should return a single semester by id', async () => {
      const semester = new SemesterEntity(
        1,
        'Semester 1',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      semesterRepositoryMock['semesters'] = [semester];
      const result = await semestersService.findOne(1);
      expect(result).toEqual(semester);
    });
    it('should throw NotFoundException if semester is not found', async () => {
      await expect(semestersService.findOne(1)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
  describe('createOne', () => {
    it('should create a new semester', async () => {
      const createSemesterDto = new CreateSemesterDto(
        'New Semester',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      await semestersService.createOne(createSemesterDto);
      const semester = semesterRepositoryMock['semesters'][0];
      expect(semester.getName()).toEqual(createSemesterDto.getName());
      expect(semester.getYear()).toEqual(createSemesterDto.getYear());
      expect(semester.getStartAt()).toEqual(createSemesterDto.getStartAt());
      expect(semester.getEndsAt()).toEqual(createSemesterDto.getEndsAt());
    });
  });
  describe('update', () => {
    it('should update an existing semester', async () => {
      const semester = new SemesterEntity(
        1,
        'Old Semester',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      semesterRepositoryMock['semesters'] = [semester];

      const updateSemesterDto = new UpdateSemesterDto(
        'Updated Semester',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      await semestersService.update(1, updateSemesterDto);

      const updatedSemester = semesterRepositoryMock['semesters'].find(
        (s) => s.getId() === 1,
      );
      expect(updatedSemester.getName()).toEqual(updateSemesterDto.getName());
      expect(updatedSemester.getYear()).toEqual(updateSemesterDto.getYear());
      expect(updatedSemester.getStartAt()).toEqual(
        updateSemesterDto.getStartAt(),
      );
      expect(updatedSemester.getEndsAt()).toEqual(
        updateSemesterDto.getEndsAt(),
      );
    });
    it('should throw NotFoundException if semester is not found', async () => {
      const updateSemesterDto = new UpdateSemesterDto(
        'Updated Semester',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      await expect(
        semestersService.update(1, updateSemesterDto),
      ).rejects.toThrow(NotFoundException);
    });
  });
  describe('remove', () => {
    it('should remove an existing semester', async () => {
      const semester = new SemesterEntity(
        1,
        'Semester to Remove',
        2024,
        new Date('2024-01-01'),
        new Date('2024-06-30'),
      );
      semesterRepositoryMock['semesters'] = [semester];
      await semestersService.remove(1);
      expect(semesterRepositoryMock['semesters'].length).toBe(0);
    });
    it('should throw NotFoundException if semester is not found', async () => {
      await expect(semestersService.remove(1)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
});
