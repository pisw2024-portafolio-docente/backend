import { IsDateString, IsInt, IsNotEmpty, IsString } from 'class-validator';

export class CreateSemesterDto {
  @IsString()
  @IsNotEmpty()
  private name: string;
  @IsInt()
  @IsNotEmpty()
  private year: number;
  @IsDateString()
  @IsNotEmpty()
  private startAt: Date;
  @IsDateString()
  @IsNotEmpty()
  private endsAt: Date;
  constructor(name: string, year: number, startAt: Date, endsAt: Date) {
    this.name = name;
    this.year = year;
    this.startAt = startAt;
    this.endsAt = endsAt;
  }
  getName(): string {
    return this.name;
  }
  getYear(): number {
    return this.year;
  }
  getStartAt(): Date {
    return this.startAt;
  }
  getEndsAt(): Date {
    return this.endsAt;
  }
}
