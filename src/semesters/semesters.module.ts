import { Module } from '@nestjs/common';
import { SemestersService } from './semesters.service';
import { SemestersController } from './semesters.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SemesterEntity } from './persistence/semester.entity';
import { SemesterRepository } from './persistence/semester.repository';

@Module({
  imports: [TypeOrmModule.forFeature([SemesterEntity])],
  controllers: [SemestersController],
  providers: [SemestersService, SemesterRepository],
})
export class SemestersModule {}
